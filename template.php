<?php
function phptemplate_links($links, $delimiter = " | ") {
  return implode($delimiter, $links);
}

function phptemplate_comment_thread_min($comment, $threshold, $pid = 0) {
  if (comment_visible($comment, $threshold)) {
    $output  = '<div style="padding-left:'. ($comment->depth * 25) ."px;\">\n";
    $output .= theme('comment_view', $comment, '', 0);
    $output .= "</div>\n";
  }
  return $output;
}

function phptemplate_comment_thread_max($comment, $threshold, $level = 0) {
  $output = '';
  if ($comment->depth) {
    $output .= '<div style="padding-left:'. ($comment->depth * 25) ."px;\">\n";
  }

  $output .= theme('comment_view', $comment, theme('links', module_invoke_all('link', 'comment', $comment, 0)), comment_visible($comment, $threshold));

  if ($comment->depth) {
    $output .= "</div>\n";
  }
  return $output;
}

function _phptemplate_variables($hook, $vars) {
  if ($hook == 'node') {
    if (module_exist("taxonomy_image")) {
       foreach (taxonomy_node_get_terms($vars['node']->nid) as $term) {
        $vars['taxonomy_images'][] = "<A HREF=\"/index.php?q=taxonomy/term/$term->tid\">".taxonomy_image_display($term->tid, "alt='$term->name'")."</A>";
       }
    }
  }
return $vars;
}

function phptemplate_image_gallery($galleries, $images) {
  return _phptemplate_callback('image_gallery', array('galleries' => $galleries, 'images' => $images));
}


drupal_add_js('misc/drupal.js');
