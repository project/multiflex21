<?php
  $mf_sb_left = theme('blocks', 'left');
  $mf_sb_right = theme('blocks', 'right');
  if ($mf_sb_left == '' && $mf_sb_right != '') {
    $num = 3;
  } else if ($mf_sb_right == '' && $mf_sb_left != '') {
    $num = 2;
  } else {
    $num = 1;
  }
?>

<div class="content<?php print $num ?>-comment">
<div class="comment <?php print ($comment->new) ? 'comment-new' : '' ?>">
<div class="comment1"><div class="comment2"><div class="comment3"><div class="comment4">
<?php if ($comment->new) : ?>
  <a id="new"></a>
  <span class="new"><?php print $new ?></span>
<?php endif; ?>

<h4 class="title"><?php print $title ?></h4>
  <?php print $picture ?>
  <div class="info">Posted by <?php print theme('username', $comment) . ' on ' . format_date($comment->timestamp) ?></div>
  <div class="content"><?php print $content ?></div>
  <?php if ($picture) : ?>
    <span class="clear"></span>
  <?php endif; ?>
  <div class="links"><?php print $links ?></div>
</div></div></div></div>
</div>
</div>
