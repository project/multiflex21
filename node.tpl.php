<?php
  $mf_sb_left = theme('blocks', 'left');
  $mf_sb_right = theme('blocks', 'right');
  if ($mf_sb_left == '' && $mf_sb_right != '') {
    $num = 3;
  } else if ($mf_sb_right == '' && $mf_sb_left != '') {
    $num = 2;
  } else {
    $num = 1;
  }
?>

                        <!-- Text container -->
                        <div class="content<?php print $num ?>-container line-box">
                                <div class="content<?php print $num ?>-container-1col">
                                        <?php if ($page == 0): ?>
					<p class="content-title-noshade-size3"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></p>
					<?php endif; ?>
                                        <p class="content-subtitle-noshade-size1">Submitted by <?php print theme('username', $node) ?> on <?php print format_date($node->created) ?></p>
                <div class="content-txtbox-noshade">
                <?php print $content ?>
                                             <?php if ($links != ''): ?>
						<p class="readmore">| <?php print $links ?> |</p>
					     <?php endif; ?>
                                        </div>
                                </div>
      </div>

