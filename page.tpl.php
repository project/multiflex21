<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <meta name="description" content="Nistix Networks" />
  <meta name="keywords" content="nistix,networks,brents,james" />
  <meta name="author" content="Nistix Networks" />
  <?php print theme('stylesheet_import', base_path() . path_to_theme() . '/css/style_screen.css', 'screen,projection');?>
  <?php print theme('stylesheet_import', base_path() . path_to_theme() . '/css/style_print.css', 'print');?>

  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
</head>

<body <?php print theme("onload_attribute"); ?>>
<?php 
  if ($sidebar_left == '' && $sidebar_right != '') {
    $multiflex_num = 3;
  } else if ($sidebar_right == '' && $sidebar_left != '') {
    $multiflex_num = 2;
  } else {
    $multiflex_num = 1;
  }
?>
  <div class="page-container-<?php print $multiflex_num ?>">

  <!-- HEADER -->
    <!-- Navigation Level 1 -->
    <?php if (is_array($primary_links) && !empty($primary_links)): ?>
    <div class="nav1-container">
      <div class="nav1">
			  <ul>
      <?php foreach ($primary_links as $link): ?>
        <li><?php print $link ?></li>
      <?php endforeach; ?>
      </ul>
		  </div>
    </div>			
  <?php endif; ?>

    <!-- Sitename -->
    <div class="site-name">
  <?php if ($site_name): ?>
      <p class="title"><a href="<?php print url(); ?>"><?php print $site_name; ?></a></p>
  <?php endif; ?>
  <?php if ($site_slogan): ?>
      <p class="subtitle"><?php print $site_slogan; ?></p>
  <?php endif;?>


    </div>
    
    <!-- Header banner -->		 						    		 						
		<div><img class="img-header" src="<?php print $logo; ?>" alt=""/></div>		
    
    <!-- Navigation Level 2 -->												
    <?php if (is_array($secondary_links) && !empty($secondary_links)): ?>
	  <div class="nav2">			
		      <ul>
      <?php foreach (array_reverse($secondary_links) as $link): ?>
        <li><?php print $link ?></li>
      <?php endforeach; ?>
      </ul>
      </div>
    <?php endif; ?>

    <!-- Buffer after header -->    
		<div class="buffer"></div>

		<!-- NAVIGATION -->				
  	<!-- Navigation Level 3 -->
		<div class="nav3">
      
      <!-- Sidebar left -->
      <?php if ($sidebar_left != ""): ?>
	<?php print $sidebar_left; ?>
      <?php endif; ?>
      </div>

  	<!-- 	CONTENT -->
		<div class="content<?php print $multiflex_num ?>">
    <?php if ($title != ""): ?>
      <div class="content<?php print $multiflex_num ?>-pagetitle"><?php print $title; ?></div>
    <?php endif; ?>
    <div class="content<?php print $multiflex_num ?>-container">

    <?php if ($tabs != ""): ?>
      <?php print $tabs; ?>
    <?php endif; ?>

    <?php if ($help != ""): ?>
      <p id="help"><?php print $help; ?></p>
    <?php endif; ?>

    <?php if ($messages != ""): ?>
      <div id="message"><?php print $messages; ?></div>
    <?php endif; ?>

      <?php print $content; ?>

      </div>
      </div>
    <!-- SIDEBAR -->		
      <?php if ($sidebar_right != ""): ?>
        <div class="sidebar">
        <?php print $sidebar_right; ?>
        </div>
      <?php endif; ?>

<?php if ($footer_message): ?>
    <!-- FOOTER -->
    <div class="footer">
       <p><b><?php print $footer_message; ?></b></p>
       <p>Design <a href="mailto:gw@actamail.com">G. Wolfgang</a> | Drupal Theme by <a href="http://www.nistix.com" title="Drupal theme created by James Brents of Nistix Networks">Nistix Networks</a> | <a href="http://validator.w3.org/check?uri=referer" title="Validate code as W3C XHTML 1.1 Strict Compliant">W3C XHTML 1.0</a> | <a href="http://jigsaw.w3.org/css-validator/" title="Validate Style Sheet as W3C CSS 2.0 Compliant">W3C CSS 2.0</a></p>
    </div>
<?php endif; ?>
  </div>
<?php print $closure;?>
</body>
</html>
